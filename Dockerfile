FROM gradle:6.0.1-jdk8

RUN mkdir -p /checker/tasks
ADD . /checker/tasks

USER root
WORKDIR /checker/tasks
#ENTRYPOINT ["/usr/bin/gradle"]
